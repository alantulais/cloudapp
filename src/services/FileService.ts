import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import axios from 'axios';

@Injectable()

export class FileService {


	public getDirectoryContent(dir){


		return axios.get('api/getDirectory/'+dir);


	}

	public createDirectory(dir){

		return axios.post('api/createDirectory/'+dir);


	 //this.client.createDirectory(dir);


	}

	public readFile(absoulutePath){

		return axios.get('api/getFile/'+absoulutePath);

	}

	

	public downloadBlob(data, fileName, mimeType) {
	  var blob, url;
	  blob = new Blob([data], {
	    type: mimeType
	  });
	  url = window.URL.createObjectURL(blob);
	  this.downloadURL(url, fileName);
	  setTimeout(function() {
	    return window.URL.revokeObjectURL(url);
	  }, 1000);
	};

	public downloadURL (data, fileName) {
	  var a;
	  a = document.createElement('a');
	  a.href = data;
	  a.download = fileName;
	  document.body.appendChild(a);
	  a.style = 'display: none';
	  a.click();
	  a.remove();
	};




}