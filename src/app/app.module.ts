import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FileService} from '../services/FileService';
import { AppComponent, LoaderComponent, DialogOverview, AlertComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
	MatListModule, 
	MatButtonModule, 
	MatCheckboxModule,
	MatIconModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatDialogRef,
  MatButton,
  MatInputModule
  } from '@angular/material';

import { MATERIAL_COMPATIBILITY_MODE } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent, LoaderComponent, AlertComponent, DialogOverview
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    MatListModule, MatIconModule,MatDialogModule, MatProgressSpinnerModule,
    FormsModule, MatButtonModule, MatInputModule
  ],
  providers: [FileService,  { provide: MATERIAL_COMPATIBILITY_MODE, useValue: true }],
  bootstrap: [AppComponent],
  entryComponents: [
    LoaderComponent,
    AlertComponent,
    DialogOverview
  ],
})
export class AppModule { }
