import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FileService } from '../services/FileService';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  routes = []; // Breadcrumb
  currentRoute = 0; // Selected item in route
  fileList = []; // Directory content
  error = false; // Show error message

  /**
 * Initialize component
 */

  constructor(
  	public fileService: FileService,
    public dialog: MatDialog
  	//public alertCtrl: AlertController,
  	//public loadingCtrl: LoadingController
  ) {

    this.addRoute("/"); //Add root route
    this.loadDirectory(this.currentRoute); // Load directory of current route

  }


 /**
 * Adds a new route to the specified dir
 */


  private addRoute(dir){

    this.routes.push(dir);
    this.currentRoute = this.routes.length - 1;

  }


/**
 * Deletes last route element
 */


  private removeLastRoute(){

    this.routes.pop();
    this.currentRoute-=1;

  }

  /**
  Load the directory at the specified index
  **/

  private loadDirectory(routeIndex) {

    // Loading message

    let $this = this;

    let loader = this.startLoad();

    // Send request to server

    this.fileService.getDirectoryContent(this.getAbsolutePath(routeIndex)).then(function(response){

      var contents = response.data;

      console.log(contents);

      $this.endLoad(loader); 

      contents.shift(); // Remove first item (current directory)

      $this.fileList = contents; // Save items

    }, function (err){

      console.log(err); 

      $this.fileList = [];

      loader.close(); // Hide loading message

      // Show error message

      $this.showAlert('No se pudo acceder al contenido');
      
	
    });


  }

/**
 * Get te absolute path as a string of the route at the specified index
 */


  getAbsolutePath(routeIndex){

    let routesTrack = this.routes.slice(0,routeIndex + 1);

    return routesTrack.join('/');

  }

/**
 * On file element's click
*/


  onClick(item){

    const $this = this;

    if (item.type === 'directory'){

      // If a directory is clicked

      this.addRoute(item.basename);
      this.loadDirectory(this.currentRoute);

    }else{

      let loader = $this.startLoad();

      $this.fileService.readFile($this.getAbsolutePath(this.currentRoute) + item.basename).then(function(response){

      var contents = response.data;

      console.log(contents);

      $this.endLoad(loader); 

    }, function (err){

      console.log(err); 

    
      loader.close(); // Hide loading message

      // Show error message

      $this.showAlert('No se pudo acceder al archivo');
  
    });
      

    }


  }

/**
 * Goes to the previous directory
*/


  goBack(){

    if (this.routes.length > 1){

      this.removeLastRoute();
      this.loadDirectory(this.currentRoute);

    }


  }


/**
 * Creates a new directory at current location
*/


  createDirectory(){

    let $this = this;

    let prompt = this.dialog.open(DialogOverview);

    prompt.componentInstance.dialogRef = prompt;

    prompt.afterClosed().subscribe(result => {

      if (result === undefined || result === '') return;

      let loader = this.startLoad();

          this.fileService.createDirectory(this.getAbsolutePath(this.currentRoute) + '/' + result).then(function(response){


            var contents = response.data;

            console.log(contents);

            $this.endLoad(loader);

            $this.reloadDirectory();
          
          }, function (err){

            console.log(err); 

            $this.showAlert('No se pudo crear el directorio');

          });
      


    });
  
  
  }

/**
 * Reloads content of current directory
*/


  reloadDirectory(){
    
    this.loadDirectory(this.currentRoute);

  }

/**
 * Displays current location
*/


  getCurrentLocation(){

    return this.routes.length > 0 ? this.routes[this.currentRoute] : '/';

  }

  /** Helper Functions
    *
  */

   startLoad(){

    let loader = this.dialog.open(LoaderComponent, {
      disableClose: true
    });

    loader.componentInstance.dialogRef = loader;

    return loader;


   }

  endLoad(loader){

      loader.close();

   }

   showAlert(message){

    let alert = this.dialog.open(AlertComponent, {
       data: {
        title: 'Ocurrió un error',
        content: message
        }
      });

     alert.componentInstance.dialogRef = alert;

     return alert;


   }



}



/* HELPER COMPONENTS
------------------------------------- */

@Component({
  selector: 'loading-screen',
  template: '<mat-spinner></mat-spinner>',
})
export class LoaderComponent {

 public dialogRef:MatDialogRef<LoaderComponent>

  constructor() { }

}

@Component({
  selector: 'alert-screen',
  template: `
<h1 mat-dialog-title>{{data.title}}</h1>
<div mat-dialog-content>
{{data.content}}
</div>

  `,
})
export class AlertComponent {

 public dialogRef:MatDialogRef<LoaderComponent>

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

}



@Component({
  selector: 'dialog-overview',
  template: `
<h1 mat-dialog-title>Nueva carpeta</h1>
<div mat-dialog-content>
  <p>Ingrese el nombre de la nueva carpeta</p>
  <mat-form-field>
    <input matInput tabindex="1" [(ngModel)]="dirname">
  </mat-form-field>
</div>
<div mat-dialog-actions>
  <button mat-button [mat-dialog-close]="dirname" tabindex="2">Crear</button>
  <button mat-button (click)="onNoClick()" tabindex="-1">Cancelar</button>
</div>

  `
})
export class DialogOverview {


  dirname = '';
  public dialogRef:MatDialogRef<DialogOverview>

  constructor(

    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}


  
