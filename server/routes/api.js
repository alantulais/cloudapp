const express = require('express');
const router = express.Router();
const createClient = require("webdav");
var fs = require("fs");

var client = createClient(
		    "https://demo.nextcloud.com/alantulais/remote.php/webdav/",
		    "prueba", // username
		    "Xprueba01X"  // password
);

/* GET api listing. */
router.get('/', (req, res) => {
  
  res.send('api works');

});

router.get('/getDirectory/*', (req, res) => {

	var dir = req.params[0];

	if (dir === undefined) dir = '/';

	client.getDirectoryContents(dir).then(function(response){

		res.setHeader('Content-Type', 'application/json');

		res.send(JSON.stringify(response, null, 3));

	}, function(err){

		console.log(err);

		res.status(500).send('Something broke!');

	});


})


router.post('/createDirectory/*', (req, res) => {

	var dir = req.params[0];

	if (dir === undefined) dir = '/';

	client.createDirectory(dir).then(function(response){

		res.setHeader('Content-Type', 'application/json');

		res.send(JSON.stringify(response, null, 3));

	}, function(err){

		console.log(err);

		res.status(500).send('Something broke!');

	});


})


router.get('/getFile/*', (req, res) => {

	var dir = req.params[0];

	if (dir === undefined) dir = '/';

	client.getFileContents(dir).then(function(fileData){

		res.setHeader('Content-Type', 'application/json');

		fs.writeFileSync(__dirname + "files/"+ dir , fileData);

		res.send({
			dest: __dirname + 'files/'+dir}
		);

	}, function(err){

		console.log(err);

		res.status(500).send('Something broke!');

	});


})

module.exports = router;
